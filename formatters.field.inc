<?php

/**
 * @file Formatters' formatters definition.
 */

/**
 * Implements hook_field_formatter_info().
 */
function formatters_field_formatter_info() {
  $formatters = array();

  foreach (formatters_formatters() as $name => $formatter) {
    // Prepare settings default values.
    $settings = array();
    foreach ($formatter['components'] as $cname => $component) {
      foreach ($component['default settings'] as $key => $val) {
        $settings[$name . '__' . $cname . '__' . $key] = $val;
      }
    }

    // Prefix formatter name to avoid conflicts and only keep the minimal needed
    // piece of data.
    $formatters['formatters__' . $name] = array(
      'label' => $formatter['label'],
      'field types' => $formatter['field types'],
      'settings' => $settings,
    );
  }

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 *
 * @todo Allow to alter formatters default settings in the future.
 */
function formatters_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();
  $element['disclaimer'] = array(
    '#markup' => '<p>' . t('The formatters settings cannot be overriden yet.') . '</p>',
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function formatters_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';

  // Use formatter name to load components then build the summary.
  $parts = explode('__', $display['type']);
  if ($parts[0] == 'formatters') {
    $fname = implode('__', array_slice($parts, 1));
    $formatter = formatters_formatter_load($fname);

    if (!empty($formatter)) {
      // For each component, show current and default settings values.
      foreach ($formatter['components'] as $cmp_key => $component) {
        if (empty($component['default settings'])) {
          continue;
        }

        $summary .= '<strong>' . t($component['component']['label']) . '</strong><br />';
        foreach ($component['default settings'] as $key => $default_setting) {
          $summary .= $key . ' = ' . $settings[$fname . '__' . $cmp_key . '__' . $key] . '<br />';
        }
      }
    }
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see formatters_formatter_run().
 */
function formatters_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  // Use formatter name to load components then build the value.
  $parts = explode('__', $display['type']);
  if ($parts[0] == 'formatters') {
    $fname = implode('__', array_slice($parts, 1));
    $formatter = formatters_formatter_load($fname);

    foreach ($items as $delta => $item) {
      $value = formatters_formatter_run($formatter, $delta, $entity_type, $entity, $field, $instance, $langcode, $items, $display);
      if (!empty($value)) {
        $element[$delta] = $value;
      }
    }
  }

  return $element;
}
