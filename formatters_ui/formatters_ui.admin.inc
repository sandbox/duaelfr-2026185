<?php


/**
 * Menu callback; Listing of all current formatters.
 */
function formatters_ui_admin_list() {
  $page = array();

  $formatters = formatters_formatters();
  $page['formatters_ui_admin_list'] = array(
    '#markup' => theme('formatters_ui_admin_list', array('formatters' => $formatters)),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'formatters') . '/formatters.admin.css' => array()),
    ),
  );

  return $page;
}


/**
 * Form builder; Edit a formatter name and components order.
 *
 * @param $form
 *   An associative array containing the current form definition.
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @param $formatter
 *   An formatter array.
 *
 * @return array
 *   An associative array containing the form definition.
 *
 * @ingroup forms
 * @see formatters_ui_formatter_form_submit()
 */
function formatters_ui_formatter_form($form, &$form_state, $formatter) {
  $title = t('Edit %label formatter', array('%label' => $formatter['label']));
  drupal_set_title($title, PASS_THROUGH);

  // Adjust this form for formatters that must be overridden to edit.
  $editable = (bool) ($formatter['storage'] & FORMATTER_STORAGE_EDITABLE);

  if (!$editable && empty($form_state['input'])) {
    drupal_set_message(t('This formatter is currently being provided by a module. Click the "Override defaults" button to change its settings.'), 'warning');
  }

  $form_state['formatter'] = $formatter;
  $form['#tree'] = TRUE;
  $form['#attached']['css'][drupal_get_path('module', 'image') . '/formatters.admin.css'] = array();

  // Show the preview.
  // @todo : preview
  /*
  $form['preview'] = array(
    '#type' => 'item',
    '#title' => t('Preview'),
    '#markup' => theme('formatters_ui_formatter_preview', array('formatter' => $formatter)),
  );
  */

  // Allow the label of the formatter to be changed
  if ($formatter['storage'] & FORMATTER_STORAGE_MODULE) {
    $form['label'] = array(
      '#type' => 'item',
      '#title' => t('Formatter label'),
      '#markup' => $formatter['label'],
      '#description' => t('This formatter is being provided by %module module and may not be renamed.', array('%module' => $formatter['module'])),
    );
  }
  else {
    $form['label'] = array(
      '#type' => 'textfield',
      '#size' => '64',
      '#title' => t('Formatter label'),
      '#default_value' => $formatter['label'],
      '#required' => TRUE,
    );
  }

  // Choose field types on which this Formatter will apply.
  $form['field_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Field types'),
    '#default_value' => (array) $formatter['field_types'],
    '#required' => TRUE,
    '#options' => array(),
  );
  foreach (field_info_field_types() as $type_name => $type) {
    $form['field_types']['#options'][$type_name] = $type['label'];
  }

  // TODO : components list

  // Show the Override or Submit button for this formatter.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['override'] = array(
    '#type' => 'submit',
    '#value' => t('Override defaults'),
    '#validate' => array(),
    '#submit' => array('formatters_ui_formatter_form_override_submit'),
    '#access' => !$editable,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update formatter'),
    '#access' => $editable,
  );

  return $form;
}

/**
 * Submit handler for overriding a module-defined formatter.
 */
function formatters_ui_formatter_form_override_submit($form, &$form_state) {
  // TODO
}


/**
 * Submit handler for saving an formatter.
 */
function formatters_ui_formatter_form_submit($form, &$form_state) {
  // Update the formatter label if it has changed.
  $formatter = $form_state['formatter'];
  if (isset($form_state['values']['label']) && $formatter['label'] != $form_state['values']['label']) {
    $formatter['label'] = $form_state['values']['label'];
  }

  // Update field types.
  // TODO check usage before removing a field type.
  $formatter['field_types'] = $form_state['values']['field_types'];

  // Update components weights.
  // TODO

  formatters_formatter_save($formatter);
  if ($form_state['values']['op'] == t('Update formatter')) {
    drupal_set_message(t('Changes to the formatter have been saved.'));
  }
  $form_state['redirect'] = 'admin/config/formatters/edit/' . $formatter['machine_name'];
}


/**
 * Form builder: Form for adding a new formatter.
 *
 * @ingroup forms
 * @see formatters_ui_formatter_add_form_submit()
 */
function formatters_ui_add_form($form, &$form_state) {
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Formatter label'),
    '#description' => t('Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#default_value' => '',
    '#maxlength' => 32,
    '#required' => TRUE,
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#description' => t('A unique machine-readable name for this webform. It must only contain lowercase letters, numbers, and underscores.'),
    '#default_value' => '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'formatters_formatter_machine_name_load',
      'source' => array('label'),
    ),
    '#element_validate' => array('formatters_ui_machine_name_validate'),
    '#required' => TRUE,
  );

  // Choose field types on which this Formatter will apply.
  $form['field_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Field types'),
    '#default_value' => array(),
    '#required' => TRUE,
    '#options' => array(),
  );
  foreach (field_info_field_types() as $type_name => $type) {
    $form['field_types']['#options'][$type_name] = $type['label'];
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new formatter'),
  );

  return $form;
}

/**
 * Submit handler for adding a formatter.
 */
function formatters_ui_add_form_submit($form, &$form_state) {
  $formatter = array(
    'label' => $form_state['values']['label'],
    'machine_name' => $form_state['values']['machine_name'],
    'field_types' => $form_state['values']['field_types'],
  );
  $formatter = formatters_formatter_save($formatter);
  drupal_set_message(t('Formatter %label was created.', array('%label' => $formatter['label'])));
  $form_state['redirect'] = 'admin/config/formatters/edit/' . $formatter['machine_name'];
}

/**
 * Element validate function to ensure unique, URL safe formatter names.
 */
function formatters_ui_machine_name_validate($element, $form_state) {
  // Check for duplicates.
  $formatters = formatters_formatters();
  if (isset($formatters[$element['#value']]) && (!isset($form_state['formatter']['ffid']) || $formatters[$element['#value']]['ffid'] != $form_state['formatter']['ffid'])) {
    form_set_error($element['#name'], t('The formatter name %name is already in use.', array('%name' => $element['#value'])));
  }
  // Check for illegal characters in formatter names.
  if (preg_match('/[^0-9a-z_\-]/', $element['#value'])) {
    form_set_error($element['#name'], t('Please only use lowercase alphanumeric characters, underscores (_), and hyphens (-) for formatter names.'));
  }
}

/**
 * Returns HTML for the page containing the list of formatters
 *
 * @param $variables
 *   An associative array containing:
 *   - formatters: An array of all the formatters
 *
 * @return string
 *   The HTML for the page containing the list of formatters.
 *
 * @ingroup themeable
 */
function theme_formatters_ui_admin_list($variables) {
  $formatters = $variables['formatters'];

  $header = array(t('Formatter label'), t('Settings'), array('data' => t('Operations'), 'colspan' => 2));
  $rows = array();
  foreach ($formatters as $formatter) {
    $row = array();
    $row[] = l($formatter['label'], 'admin/config/formatters/edit/' . $formatter['machine_name']);
    $link_attributes = array(
      'attributes' => array('class' => array('formatter-link'))
    );
    if ($formatter['storage'] == ENTITY_CUSTOM) {
      $row[] = t('Custom');
      $row[] = l(t('edit'), 'admin/config/formatters/edit/' . $formatter['machine_name'], $link_attributes);
      $row[] = l(t('delete'), 'admin/config/formatters/delete/' . $formatter['machine_name'], $link_attributes);
    }
    elseif ($formatter['storage'] == ENTITY_OVERRIDDEN) {
      $row[] = t('Overridden');
      $row[] = l(t('edit'), 'admin/config/formatters/edit/' . $formatter['machine_name'], $link_attributes);
      $row[] = l(t('revert'), 'admin/config/formatters/revert/' . $formatter['machine_name'], $link_attributes);
    }
    else {
      $row[] = t('Default');
      $row[] = l(t('edit'), 'admin/config/formatters/edit/' . $formatter['machine_name']);
      $row[] = '';
    }
    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(array(
      'colspan' => 4,
      'data' => t('There are currently no formatters. <a href="!url">Add a new one</a>.', array('!url' => url('admin/config/formatters/add'))),
    ));
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}
