<?php

/**
 * @file
 * Hooks provided by the Formatters module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define new components to be used by Formatters.
 *
 * @return array
 *   An array of components keyed by machine name.
 *   Each component should contain :
 *   - "label" (required): The component human-readable name
 *   - "help" (optional): A help text
 *   - "process callback" (required): The function which will alter the field
 *     data
 *   - "options callback" (optional): -
 *   - "form callback" (optional): the form used to display the options
 * @todo Complete this documentation.
 */
function hook_formatters_component_info() {
  $components = array();

  $components['trim'] = array(
    'label' => t('Trim'),
    'help' => t('Trim the input to a given length.'),
    'process callback' => 'trim_process',
    'options callback' => 'trim_options',
    'form callback' => 'trim_form',
    'file' => drupal_get_path('module', 'formatters') . '/includes/component.trim.inc',
  );

  return $components;
}

/**
 * Alter existing components.
 *
 * @param $components
 *   The existing components.
 * @see hook_formatters_component_info()
 */
function hook_formatters_component_info_alter(&$components) {
  $components['trim']['process callback'] = 'my_trim_process';
}

/**
 * Define new Formatters.
 *
 * @return array
 *   An array of formatters keyed by machine name.
 *   Each formatter should contain :
 *   - label (required): The formatter human-readable name
 *   - field types (required): The field types on which the formatter will apply
 *   - components (required): The formatter components
 * @todo Complete this documentation.
 */
function hook_formatters_default_formatters() {
  $formatters = array();

  $formatters['trimmed_plain'] = array(
    'label' => t('Trimmed plain'),
    'field types' => array(
      'text', 'text_long', 'text_with_summary',
    ),
    'components' => array(
      'plain' => array(
        'component' => 'plain',
        'default settings' => array(),
      ),
      'trim' => array(
        'component' => 'trim',
        'default settings' => array('length' => 100),
      ),
    )
  );

  return $formatters;
}

/**
 * Alter existing Formatters.
 *
 * @param $formatters
 *   The existing Formatters.
 * @see hook_formatters_default_formatters()
 */
function hook_formatters_default_formatters_alter(&$formatters) {
  $formatters['trimmed_plain']['field types'] = array('text');
  $formatters['trimmed_plain']['components']['trim']['default settings']['length'] = 150;
}

/**
 * @} End of "addtogroup hooks".
 */
