This module allows site builders to easily define new field Formatters without
having to write code. Each Formatter is a set of simple configurable components
which will be called one by one to generate the output.

It is highly inspired by the core Image module.

------------
Dependencies
------------
Entity API

-------------
How to use it
-------------
1. Install and enable as usual.
2. Go to admin/structure/formatters.
3. Create a new Formatter, fill its label and choose on which field types it will
apply.
4. Add one or many components to your Formatter and define their default
settings.
5. Go to your entity display settings and select the new Formatter you want to
apply on your field. You can override components' settings that have been
specified previously.
6. Enjoy \o/

-------
Credits
-------
This module('s very early version) have been made in 5 hours during the Drupal
Camp Paris 2013 by bdecarne, DuaelFr, FMB, Haza, Kgaut, KroKroDile, Liaz and
Y.sa.