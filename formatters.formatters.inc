<?php

/**
 * @file
 * Functions needed to execute image effects provided by Image module.
 */

/**
 * Implements hook_formatters_component_info().
 */
function formatters_formatters_component_info() {
  $components = array();

  $components['trim'] = array(
    'label' => t('Trim'),
    'help' => t('Trim the input to a given length.'),
    'process callback' => 'trim_process',
    'options callback' => 'trim_options',
    'form callback' => 'trim_form',
    'file' => drupal_get_path('module', 'formatters') . '/includes/component.trim.inc',
  );

  return $components;
}

/**
 * Implements hook_formatters_default_formatters().
 */
function formatters_formatters_default_formatters() {
  $formatters = array();

  $formatters['trim_to_50'] = array(
    'label' => t('Trim to 50'),
    'field types' => array(
      'text', 'text_long', 'text_with_summary',
    ),
    'components' => array(
      'trim' => array(
        'component' => 'trim',
        'default settings' => array('length' => 50),
      ),
    ),
  );
  $formatters['trim_to_100'] = array(
    'label' => t('Trim to 100'),
    'field types' => array(
      'text', 'text_long', 'text_with_summary',
    ),
    'components' => array(
      'trim' => array(
        'component' => 'trim',
        'default settings' => array('length' => 100),
      ),
    ),
  );

  return $formatters;
}
